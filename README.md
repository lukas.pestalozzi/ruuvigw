# ruuvigw
![ruuvigw picture](https://user-images.githubusercontent.com/2929136/115986648-eab20480-a5b9-11eb-98be-b840e62dc9c1.png)
Ruuvi gateway with InfluxDB and Grafana. Works with https://ruuvi.com/ tags.  
Tested to work at least with Raspberry Pi Zero.

Run:
```
apt install ansible git -y
git clone https://github.com/ilatvala/ruuvigw.git
cd ruuvigw/
```
Modify the MAC addresses of your ruuvi tags to the ruuvinames var part in the beginning of the ruuvigw.yml playbook (if you know what they are)  
You can also modify those later in /etc/RuuviCollector/target/ruuvi-names.properties

Run playbook with:
```
ansible-playbook -i "127.0.0.1," ruuvigw.yml
```
If you get any errors, then just run the playbook again, or do the step manually and continue with the next:
```
ansible-playbook -i "127.0.0.1," ruuvigw.yml --start-at-task="Task name"
```

Reboot after installation.  
Grafana should be available in port 80 (admin/admin) with some default Ruuvi dashboards.

## Configuration after installation:

### RuuviCollector

Set measurement update limit to a reasonable duration (unit ms)
```
measurementUpdateLimit=120000
```

### Grafana
Enable anonymous view access for grafana so you don’t have to sign in to view the data. Open /etc/grafana/grafana.ini in text editor and enable the anonymous view.
```
### Anonymous Auth ###
[auth.anonymous]
# enable anonymous access
enabled = true
```


### InfluxDB
/etc/influxdb/influxdb.conf: Disable internal Monitoring (to save CPU)

```
[monitor]
store-enabled = false
```
